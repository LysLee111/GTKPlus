#include <cairo.h> 
#include <cairo-pdf.h> //for pdf surface
#include <cairo-svg.h> //for svg surface

int main(void) {
    cairo_surface_t *surf;
    cairo_t *cr;
    #ifdef TOPNG
    //创建图像类型的surface, 图片大小为320x48
    surf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 320, 48);
    #elif defined(TOPDF)
    //创建pdf类型的surface
    surf = cairo_pdf_surface_create("cr2pdf.pdf", 320, 48);
    #elif defined(TOSVG)
    surf = cairo_svg_surface_create("cr2svg.svg", 320, 48);
    #endif
    //基于surface创建cairo环境(句柄)
    cr = cairo_create(surf);
    //设置source的颜色为暗红(相当于选择了暗红色画笔)
    cairo_set_source_rgb(cr, 0.627, 0, 0);
    //设置字体
    //fc-list :lang=zh, 列出系统安装的中文字体
    cairo_select_font_face(cr, "Noto Sans Mono CJK HK", CAIRO_FONT_SLANT_NORMAL, 
                               CAIRO_FONT_WEIGHT_NORMAL);
    //设置字体大小
    cairo_set_font_size(cr, 24.0);
    //(将画笔)移动到图片的(10, 34)位置，相对于图片左上角 
    cairo_move_to(cr, 10.0, 34.0);
    //开始绘制文字
    cairo_show_text(cr, "我是中国人，我爱我的祖国。");
    #ifdef TOPNG
    //将图片surface写到png文件
    cairo_surface_write_to_png(surf, "cr2png.png");
    #elif defined(TOPDF) || defined(TOSVG)
    //将surface写入到pdf/svg文件
    cairo_show_page(cr);
    #endif
    //svg 文件会自动写入?
    //销毁cairo环境
    cairo_destroy(cr); 
    //销毁surface
    cairo_surface_destroy(surf);
    return 0;
}
