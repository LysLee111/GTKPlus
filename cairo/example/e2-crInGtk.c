#include <cairo.h>
#include <gtk/gtk.h>

static gboolean on_expose_event(GtkWidget *wid, GdkEventExpose *evt, gpointer data) {
    cairo_t *cr;
    //基于gdk创建cr
    cr = gdk_cairo_create(wid->window); 
    //设置source的颜色为暗红(相当于选择了暗红色画笔)
    cairo_set_source_rgb(cr, 0.627, 0, 0);
    //设置字体
    //fc-list :lang=zh, 列出系统安装的中文字体
    cairo_select_font_face(cr, "Noto Sans Mono CJK HK", CAIRO_FONT_SLANT_NORMAL, 
                               CAIRO_FONT_WEIGHT_NORMAL);
    //设置字体大小
    cairo_set_font_size(cr, 24.0);
    //(将画笔)移动到图片的(10, 34)位置，相对于图片左上角 
    cairo_move_to(cr, 10.0, 34.0);
    //开始绘制文字
    cairo_show_text(cr, "我是中国人，我爱我的祖国。");
    //绘制完成后即可销毁
    cairo_destroy(cr); 
    
}

int main(int argc, char *argv[]) {
    GtkWidget *win;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect(win, "expose-event", G_CALLBACK(on_expose_event), NULL);
    g_singal_connect(win, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(win), 320, 48);
    gtk_widget_show_all(win);
    gtk_main();
    return 0;
}
