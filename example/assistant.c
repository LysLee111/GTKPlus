#include <gtk/gtk.h>
#include <string.h>

static void gtk_assistant_set_current_page_complete(GtkAssistant *assist,
    gboolean complete);
static void name_changed(GtkEntry *name, GtkAssistant *assist);
static void check_toggled(GtkToggleButton *check, GtkAssistant *assist);
static GtkWidget *get_current_page(GtkAssistant *assist);
static void pbtn_clicked(GtkWidget *btn, GtkAssistant *assist);
static void assist_cancelled(GtkWidget *assist, gpointer data);
static void assist_closed(GtkWidget *assist, GtkEntry *ename);

int main(int argc, char *argv[])
{
    GtkWidget *assist, *intro, *hbname, *lname, *ename, *check, *hbprogress,
              *pbar, *pbtn, *confirm;
    gtk_init(&argc, &argv);
    assist = gtk_assistant_new();
    intro = gtk_label_new("This is an example of a GtkAssistant.");
    hbname = gtk_hbox_new(FALSE, 5);
    lname = gtk_label_new("Your name: ");
    ename = gtk_entry_new();
    check = gtk_check_button_new_with_label("Cick Me To Continue!");
    hbprogress = gtk_hbox_new(FALSE, 0);
    pbar = gtk_progress_bar_new();
    pbtn = gtk_button_new_with_label("Click me!");
    confirm = gtk_label_new("Text has been entered in the label and the\n"
                            "combo box is clicked. If you are done, then\n"
                            "it is time to leave!");
    gtk_assistant_append_page(GTK_ASSISTANT(assist), intro);
    gtk_assistant_append_page(GTK_ASSISTANT(assist), hbname);
    gtk_box_pack_start_defaults(GTK_BOX(hbname), lname);
    gtk_box_pack_start_defaults(GTK_BOX(hbname), ename);
    gtk_assistant_append_page(GTK_ASSISTANT(assist), check);
    gtk_assistant_append_page(GTK_ASSISTANT(assist), hbprogress);
    gtk_box_pack_start_defaults(GTK_BOX(hbprogress), pbar);
    gtk_box_pack_start_defaults(GTK_BOX(hbprogress), pbtn);
    gtk_assistant_append_page(GTK_ASSISTANT(assist), confirm);
    gtk_assistant_set_page_complete(GTK_ASSISTANT(assist), intro, TRUE);
    gtk_assistant_set_page_complete(GTK_ASSISTANT(assist), confirm, TRUE);
    gtk_assistant_set_page_type(GTK_ASSISTANT(assist), intro,
                                GTK_ASSISTANT_PAGE_INTRO);
    gtk_assistant_set_page_type(GTK_ASSISTANT(assist), hbprogress,
                                GTK_ASSISTANT_PAGE_PROGRESS);
    gtk_assistant_set_page_type(GTK_ASSISTANT(assist), confirm,
                                GTK_ASSISTANT_PAGE_CONFIRM);
    gtk_assistant_set_page_title(GTK_ASSISTANT(assist), intro, "Introduction");
    gtk_assistant_set_page_title(GTK_ASSISTANT(assist), hbname, "Name");
    gtk_assistant_set_page_title(GTK_ASSISTANT(assist), check, "Check");
    gtk_assistant_set_page_title(GTK_ASSISTANT(assist), hbprogress, "Progress");
    gtk_assistant_set_page_title(GTK_ASSISTANT(assist), confirm, "Confirm");
    g_object_set_data(G_OBJECT(hbprogress), "pbar", pbar);
    //gtk_progress_bar_set_pulse_step(pbar, 0.05);
    g_signal_connect(G_OBJECT(ename), "changed", 
                     G_CALLBACK(name_changed), assist);
    g_signal_connect(G_OBJECT(check), "toggled", 
                     G_CALLBACK(check_toggled), assist);
    g_signal_connect(G_OBJECT(pbtn), "clicked", 
                     G_CALLBACK(pbtn_clicked), assist);
    g_signal_connect(G_OBJECT(assist), "close",
                     G_CALLBACK(assist_closed), ename);
    g_signal_connect(G_OBJECT(assist), "cancel",
                     G_CALLBACK(assist_cancelled), NULL);
    gtk_widget_show_all(assist);
    gtk_main();
    return 0;
    
}
static void pbtn_clicked(GtkWidget *btn, GtkAssistant *assist) {
    GtkProgressBar *pbar;
    GtkWidget *hbp;
    gchar pmsg[20];
    gdouble percent;
    gtk_widget_set_sensitive(btn, FALSE);
    hbp =  get_current_page(assist);
    pbar = GTK_PROGRESS_BAR(g_object_get_data(G_OBJECT(hbp), "pbar"));
    percent = 0;
    while(percent < 100) {
        g_usleep(500000);
        percent += 5;
        gtk_progress_bar_set_fraction(pbar, percent/100.0);
        g_snprintf(pmsg, 20, "%.0f%% Complete", percent);
        gtk_progress_bar_set_text(pbar, pmsg);
        while(gtk_events_pending()) {
            gtk_main_iteration_do(FALSE);
        }
    }
    gtk_assistant_set_page_complete(assist, hbp, TRUE);
}

static void assist_cancelled(GtkWidget *assist, gpointer data) {
    gtk_widget_destroy(assist);
    gtk_main_quit();
} 
static void assist_closed(GtkWidget *assist, GtkEntry *ename) {
    g_message("Your name: %s", gtk_entry_get_text(ename));
    assist_cancelled(assist, NULL);
} 

static GtkWidget *get_current_page(GtkAssistant *assist) {
    GtkWidget *page;
    gint num;
    num = gtk_assistant_get_current_page(assist);
    page = gtk_assistant_get_nth_page(assist, num);
    return page;
}

static void gtk_assistant_set_current_page_complete(GtkAssistant *assist, gboolean complete) {
    GtkWidget *page;
    page = get_current_page(assist);
    gtk_assistant_set_page_complete(assist, page, complete);
}

static void name_changed(GtkEntry *name, GtkAssistant *assist) {
    gchar *text;
    text = g_strdup(gtk_entry_get_text(name));
    gtk_assistant_set_current_page_complete(assist, strlen(g_strstrip(text))!=0);
    g_free(text);
}

static void check_toggled(GtkToggleButton *check, GtkAssistant *assist) {
    gtk_assistant_set_current_page_complete(assist, 
        gtk_toggle_button_get_active(check));
}
