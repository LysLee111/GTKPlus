#include <gtk/gtk.h>

#ifndef MODAL
#define MODAL 1
#endif

#ifndef TRANSIENT
#define TRANSIENT FALSE
#endif

static void show_dialog(GtkWindow *btn, gpointer data);

static void response_cb(GtkWidget *dialog, gint res_id, gpointer data);

int main(int argc, char *argv[])
{
    GtkWidget *win, *button;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    button = gtk_button_new_with_label("Dialog");
    gtk_container_add(GTK_CONTAINER(win), button);
    g_signal_connect_swapped(G_OBJECT(button), "clicked",
                     G_CALLBACK(show_dialog), win);
    gtk_widget_set_size_request(win, 200, -1);
    gtk_widget_show_all(win); 
    gtk_main();
    return 0;
    
}


static void show_dialog(GtkWindow *win, gpointer data) {
    GtkWidget *dialog, *hbox, *image, *label;
    dialog = gtk_dialog_new_with_buttons("Dialog", win, 0
                                     #if MODAL  == 1  
                                         | GTK_DIALOG_MODAL 
                                     #endif
                                         ,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
    hbox = gtk_hbox_new(TRUE, 5);
    image = gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
    label = gtk_label_new("The button was clickded!");
    gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox);
    gtk_box_pack_start_defaults(GTK_BOX(hbox), image);
    gtk_box_pack_start_defaults(GTK_BOX(hbox), label);

#ifndef DIALOG_RUN
    g_signal_connect(G_OBJECT(dialog), "response",
                     G_CALLBACK(response_cb), NULL);
#if TRANSIENT
    gtk_window_set_transient_for(GTK_WINDOW(dialog), win);
#endif //TRANSIENT
#endif // DIALOG_RUN

    gtk_widget_show_all(dialog);

#ifdef DIALOG_RUN
    gint res_id = 0;
    res_id = gtk_dialog_run(GTK_DIALOG(dialog));
    response_cb(dialog, res_id, NULL);
#endif
 
}

static void response_cb(GtkWidget *dialog, gint res_id, gpointer data) {
    switch(res_id) {
    default:
        g_print("switch keyword default test\n");
    case GTK_RESPONSE_OK:
        g_print("got response id OK\n");
        
    }
    gtk_widget_destroy(dialog);
}
