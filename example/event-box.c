#include <gtk/gtk.h>

static void button_press_cb(GtkEventBox *evbox, GdkEventButton *btnev, 
                            GtkLabel *label);
static void btn1_clicked_cb(GtkButton *btn, gpointer data);
static void btn2_clicked_cb(GtkButton *btn, gpointer data);

int main(int argc, char *argv[])
{
    GtkWidget *win, *evbox, *table, *btn1, *btn2, *label;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    evbox = gtk_event_box_new();
    table = gtk_table_new(3, 3, TRUE);
    btn1 = gtk_button_new();
    btn2 = gtk_button_new();
    label = gtk_label_new("Try to dobule click me!");
    gtk_widget_set_size_request(win, 600, 480);
    gtk_widget_set_events(evbox, GDK_BUTTON_PRESS_MASK);
    gtk_event_box_set_above_child(GTK_EVENT_BOX(evbox), TRUE);
    gtk_container_add(GTK_CONTAINER(win), evbox);
    gtk_widget_realize(evbox);
    gdk_window_set_cursor(evbox->window, gdk_cursor_new(GDK_HAND1));
    gtk_container_set_border_width(GTK_CONTAINER(win), 10);
    gtk_container_add(GTK_CONTAINER(evbox), table);
    gtk_container_set_border_width(GTK_CONTAINER(evbox), 10);
    gtk_table_attach_defaults(GTK_TABLE(table), btn1, 1, 2, 1, 2);
    gtk_container_add(GTK_CONTAINER(btn1), btn2);
    gtk_container_set_border_width(GTK_CONTAINER(btn1), 10);
    //gtk_container_add(GTK_CONTAINER(evbox), label);
    gtk_container_add(GTK_CONTAINER(btn2), label);
    g_signal_connect(G_OBJECT(evbox), "button-press-event", G_CALLBACK(button_press_cb), label);
    g_signal_connect(G_OBJECT(btn1), "clicked", G_CALLBACK(btn1_clicked_cb), NULL);
    g_signal_connect(G_OBJECT(btn2), "clicked", G_CALLBACK(btn2_clicked_cb), NULL);
    gtk_widget_show_all(win);
    gtk_main();
    return 0;   
}

static void button_press_cb(GtkEventBox *evbox, GdkEventButton *btn_ev, 
                            GtkLabel *label) {
    gchar const *text;
    if(btn_ev->type == GDK_2BUTTON_PRESS) {
        text = gtk_label_get_text(label);
        if(*text == 'T') {
            gtk_label_set_text(label, "Double click me again!");
        }else {
            gtk_label_set_text(label, "Try to double click me");
        }
    }
}

static void btn1_clicked_cb(GtkButton *btn, gpointer data) {
    g_print("The btn1 handled the clicked signal\n");
}

static void btn2_clicked_cb(GtkButton *btn, gpointer data) {
    g_print("The btn2 handled the clicked signal\n");
}
