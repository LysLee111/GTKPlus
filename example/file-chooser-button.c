#include <gtk/gtk.h>

static void folder_changed(GtkFileChooser *folder_chooser,
                           GtkFileChooser *file_chooser);
static void file_changed(GtkFileChooser *file_chooser,
                           GtkLabel *label);

int main(int argc, char *argv[])
{
    GtkWidget *win, *vbox, *folder_chooser, *file_chooser, *label;
    GtkFileFilter *filter1, *filter2;
    gchar const *home_dir;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    vbox = gtk_vbox_new(TRUE, 5);
    folder_chooser = gtk_file_chooser_button_new("Folder Chooser", 
        GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
    file_chooser = gtk_file_chooser_button_new("File Chooser",
        GTK_FILE_CHOOSER_ACTION_OPEN);
    label = gtk_label_new("");
    gtk_container_add(GTK_CONTAINER(win), vbox);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), folder_chooser);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), file_chooser);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), label);
    g_signal_connect(G_OBJECT(folder_chooser), "selection-changed", 
                             G_CALLBACK(folder_changed), file_chooser);    
    g_signal_connect(G_OBJECT(file_chooser), "selection-changed", 
                             G_CALLBACK(file_changed), label);    
    home_dir = g_get_home_dir();
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(folder_chooser),
                                               home_dir);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser),
                                               home_dir);
    g_free((gpointer)home_dir);
    filter1 = gtk_file_filter_new();
    filter2 = gtk_file_filter_new();
    gtk_file_filter_set_name(filter1, "All files");
    gtk_file_filter_set_name(filter2, "Image files");
    gtk_file_filter_add_pattern(filter1, "*");
    gtk_file_filter_add_pattern(filter2, "*.png");
    gtk_file_filter_add_pattern(filter2, "*.jpg");
    gtk_file_filter_add_pattern(filter2, "*.gif");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(file_chooser), filter1);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(file_chooser), filter2);
    gtk_widget_show_all(win);
    gtk_main();
    return 0;   
}

static void folder_changed(GtkFileChooser *folder_chooser,
                           GtkFileChooser *file_chooser) {
    gchar *dir;
    dir = gtk_file_chooser_get_filename(folder_chooser);
    gtk_file_chooser_set_current_folder(file_chooser, dir);
    g_free(dir);
    dir = gtk_file_chooser_get_filename(file_chooser);
    g_print("The current file chooser file is: %s\n", dir);
    g_free(dir);
}

static void file_changed(GtkFileChooser *file_chooser,
                           GtkLabel *label) {
    gchar *text;
    text = gtk_file_chooser_get_filename(file_chooser);
    gtk_label_set_text(label, text);
    g_free(text);
}
