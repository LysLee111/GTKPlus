#include <glib.h>
#include <gmodule.h>

int main(int argc, char *argv[]) {
    gboolean (*print_msg1)(gchar const *data);
    gboolean (*print_msg2)(gchar const *data);
    GModule *module;
    //gchar const *md_name = "/usr/lib64/gm-plugin.so";
    //gchar const *md_name = "gm-plugin.so";
    gchar const *md_name = "gm-plugin";
    g_assert(g_module_supported());
    g_print("G_MODULE_SUFFIX:%s\n", G_MODULE_SUFFIX);
    module = g_module_open(md_name, G_MODULE_BIND_LAZY);
    if(!module) {
        g_error("open module %s failed!", md_name);
    }
    if(!g_module_symbol(module, "print_msg1", (gpointer*)&print_msg1)) {
        g_error("resolve symbol print_msg1 failed");
    }
    if(!g_module_symbol(module, "print_msg2", (gpointer*)&print_msg2)) {
        g_error("resolve symbol print_msg2 failed");
    }
    print_msg1("this is msg1.\n");
    print_msg2("this is msg2.\n");
    if(!g_module_close(module)) {
        g_error("close module failed");
    }
    return 0;
}
