#include <gmodule.h>
#include <glib.h>

G_MODULE_EXPORT gboolean print_msg1(gchar const *data);
G_MODULE_EXPORT void print_msg2(gchar const *data);
gboolean print_msg1(gchar const *data) {
    g_print("Calling print_msg1:\n\t%s", data);
    return TRUE;
}

void print_msg2(gchar const *data) {
    g_print("Calling print_msg2:\n\t%s", data);
}
