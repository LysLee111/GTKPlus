#include <gtk/gtk.h>

static void button_press_cb(GtkEventBox *evbox, GdkEventButton *btnev, 
                            GtkLabel *label);
static void btn1_clicked_cb(GtkButton *btn, gpointer data);
static void btn2_clicked_cb(GtkButton *btn, gpointer data);

int main(int argc, char *argv[])
{
    GtkWidget *win, *entry;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    entry = gtk_entry_new();
    gtk_widget_set_size_request(win, 600, 480);
    gtk_container_add(GTK_CONTAINER(win), entry);
    //gtk_widget_realize(evbox);
    gtk_widget_show_all(win);
    gtk_main();
    return 0;   
}

