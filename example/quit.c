#include <gtk/gtk.h>

int main(int argc, char *argv[])
{
    GtkWidget *win, *button;
    gtk_init(&argc, &argv); 
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);   
    button = gtk_button_new_with_label("Hello World!");
    gtk_container_add(GTK_CONTAINER(win), button);
    g_signal_connect(G_OBJECT(button), "clicked",
                     G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(G_OBJECT(win), "destroy", 
                     G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_show_all(win);
    gtk_main();
    return 0;
}
