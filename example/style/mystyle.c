#include <gtk/gtk.h>

int main(int argc, char *argv[])
{
    GtkWidget *win, *vbox, *label, *btn, *check_btn, *tgl_btn;
    gtk_init(&argc, &argv);
    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    vbox = gtk_vbox_new(TRUE, 5);
    label = gtk_label_new("GtkLabel");
    btn = gtk_button_new_with_label("GtkButton");
    check_btn = gtk_check_button_new_with_label("GtkCheckButton");
    tgl_btn = gtk_toggle_button_new_with_label("GtkToggleButton");
    gtk_container_add(GTK_CONTAINER(win), vbox);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), label);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), btn);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), check_btn);
    gtk_box_pack_start_defaults(GTK_BOX(vbox), tgl_btn);
    gtk_label_set_selectable(GTK_LABEL(label), TRUE);
    gtk_rc_parse("mystyle.gtkrc");
    gtk_widget_show_all(win);
    gtk_main();
    return 0;
}
